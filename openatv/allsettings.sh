#!/bin/sh

if grep -qs -i "openATV" /etc/image-version; then

#check python version
python=$(python -c "import platform; print(platform.python_version())")
sleep 1;
case $python in 
3.11.0|3.11.1|3.11.2|3.11.3|3.11.4|3.11.5|3.11.6) -O - | /bin/sh
wget -q "--no-check-certificate" https://gitlab.com/eliesat/images/-/raw/main/openatv/settings-7.3.4.sh
;;
3.12.0|3.12.1|3.12.2|3.12.3|3.12.4|3.12.5|3.12.6)
wget -q "--no-check-certificate" https://gitlab.com/eliesat/images/-/raw/main/openatv/settings-7.4.4.sh -O - | /bin/sh
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac
else
echo "> your image isnt yet supported"
fi
