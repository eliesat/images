#!/bin/bash
if [ -f /etc/opkg/eliesat-feed.conf ]; then
eliesatfeed=installed
else
echo "> Run new eliesatpanel installation script and try again ..."
exit 1
fi

# Determine package manager
if command -v dpkg &> /dev/null; then
    exit 1
    package_manager="apt"
    status_file="/var/lib/dpkg/status"
    install_command="dpkg -i --force-overwrite"
    uninstall_command="apt-get purge --auto-remove -y"
    ostype="Oe 2.5/2.6"
else
    package_manager="opkg"
    install_command="opkg install --force-reinstall"
    uninstall_command="opkg remove --force-depends"
    status_file="/var/lib/opkg/status"
    ostype="Oe 2.0"
fi

# Functions
print_message() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

cleanup() {

    rm -rf /var/cache/opkg/* /var/lib/opkg/lists/* /run/opkg.lock $i /tmp/tmp >/dev/null 2>&1
}

image_version="/etc/image-version"
box_type=$(head -n 1 /etc/hostname)
distro_value=$(grep '^distro=' "$image_version" | awk -F '=' '{print $2}')
distro_version=$(grep '^version=' "$image_version" | awk -F '=' '{print $2}')
python_version=$(python --version 2>&1 | sed 's/[^ ]* //')

if [[ "$distro_value" == "openatv" ]] && [[ $distro_version == "7.5.1" ]]; then
print_message "> Image : $distro_value-$distro_version"
sleep 1
print_message "> Python : $python_version"
sleep 1
echo
else
print_message "> Your image is not supported yet, Install openatv 7.5.1 image and try again..."
exit 1
fi

# Determine backup feed plugins
plugins_to_install=(
astra-sm
enigma2-plugin-extensions-e2iplayer-deps
enigma2-plugin-extensions-epgimport
enigma2-plugin-extensions-epgtranslator
enigma2-plugin-extensions-ipchecker
enigma2-plugin-extensions-oaweather
enigma2-plugin-extensions-permanentclock
enigma2-plugin-extensions-setpicon
enigma2-plugin-extensions-tmdb
enigma2-plugin-systemplugins-autoresolution
enigma2-plugin-systemplugins-weathercomponenthandler
enigma2-plugin-subscription-ai-powered-subtitle-translation
enigma2-plugin-skins-metrix-atv-fhd-icons
enigma2-plugin-softcams-ncam
enigma2-plugin-weblinks-arabicsavior
enigma2-plugin-weblinks-astrasm
enigma2-plugin-weblinks-bitrate
#enigma2-plugin-weblinks-bootlogoswapper
enigma2-plugin-weblinks-cacheflush
enigma2-plugin-weblinks-channels
enigma2-plugin-weblinks-chocholousek-picons
enigma2-plugin-weblinks-crashlogviewer
enigma2-plugin-weblinks-e2iplayer
enigma2-plugin-weblinks-epggrabber
enigma2-plugin-weblinks-footonsat
enigma2-plugin-weblinks-freecccamscript
enigma2-plugin-weblinks-historyzapselector
enigma2-plugin-weblinks-internet-speedtest
enigma2-plugin-weblinks-ipaudiopro
enigma2-plugin-weblinks-iptosat
enigma2-plugin-weblinks-jedimakerxtream
enigma2-plugin-weblinks-keyadder
enigma2-plugin-weblinks-keysmnasr
enigma2-plugin-weblinks-mymetrixlitebackup
enigma2-plugin-weblinks-multistalkerpro
enigma2-plugin-weblinks-newvirtualkeyboard
enigma2-plugin-weblinks-raedquicksignal
enigma2-plugin-weblinks-sherlockmod
enigma2-plugin-weblinks-skinparts-mnasr
enigma2-plugin-weblinks-softcamsecretfeed
enigma2-plugin-weblinks-spinneratv
enigma2-plugin-weblinks-subssupport
enigma2-plugin-weblinks-tssateditor
enigma2-plugin-weblinks-xklass
enigma2-plugin-weblinks-xtraevent
enigma2-plugin-weblinks-xstreamity
)

check_and_install_package() {
    if ! grep -q "Package: $1" "$status_file"; then
    $uninstall_command $1 >/dev/null 2>&1
    opkg update > /dev/null 2>&1
    print_message "Downloading $1, please wait..."
    opkg download $1 > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        print_message "Failed to download $1 from feed"
    fi
    print_message "Installing $1, please wait..."
    $install_command $1 >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        print_message "$1 installed successfully."
    else
        print_message "Installation of  $1 failed."
    fi
   else
   print_message "$1 package already installed"
   sleep 2
   fi
}

for i in "${plugins_to_install[@]}"; do
    cleanup
    check_and_install_package $i
    echo
    rm -rf *.ipk >/dev/null 2>&1
    sleep 1
done

sleep 1
sh -c "$(wget --no-check-certificate  https://gitlab.com/eliesat/images/-/raw/main/openatv/openatv-7.5-settings.sh -qO -)"