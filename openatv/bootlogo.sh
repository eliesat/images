#!/bin/sh

echo "> downloading & installing bootlogo file Please Wait ..."
sleep 3

rm -rf /usr/lib/enigma2/python/Plugins/Extensions/BootLogoSwapper/bootlogos/* > /dev/null 2>&1
rm -rf /tmp/bootlogo.mvi > /dev/null 2>&1

wget --show-progress -qO /tmp/bootlogo.mvi "https://gitlab.com/eliesat/images/-/raw/main/openatv/bootlogo.mvi"
download=$?
echo ''
if [ $download -eq 0 ]; then
cp -rf /tmp/bootlogo.mvi /usr/share/backdrop.mvi > /dev/null 2>&1
cp -rf /tmp/bootlogo.mvi /usr/share/bootlogo.mvi > /dev/null 2>&1
rm -rf /tmp/bootlogo.mvi > /dev/null 2>&1
echo "> installation of bootlogo file finished"
sleep 3
else
echo "> installation of bootlogo file failed"
fi










