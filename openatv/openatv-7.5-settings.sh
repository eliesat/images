#!/bin/sh

#check image type
if [ -f /etc/apt/apt.conf ]; then
status='/var/lib/dpkg/status'
it=DreamOs
else
status='/var/lib/opkg/status'
it=OpenSource
fi

# Check  internet connectivity 
if ping -q -c 1 -W 1 google.com >/dev/null; then
echo ""
sleep 3
else
echo "> internet:no internet connection"
if [ ! -f /etc/resolv-backup.conf ] 
then
grep "nameserver.*" /etc/resolv.conf >> //etc/resolv-backup.conf
fi
> /etc/resolv.conf
echo "nameserver 8.8.8.8" > /etc/resolv.conf; echo "nameserver 8.8.4.4" >> /etc/resolv.conf; echo "> done"
echo "> your device will restart now please wait..."
init 4
sleep 2
mv /tmp/file.txt /etc/enigma2/settings
if [ "$it" = "DreamOS" ]; then
sleep 2s
systemctl restart enigma2
else
sleep 2
init 3
fi
fi

#check and install metrix icons from image feed
package="enigma2-plugin-skins-metrix-atv-fhd-icons"
# Determine package manager
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
install_command="apt-get install -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
install_command="opkg install --force-reinstall"
fi
if grep -q "$package" "$status_file"; then
echo ""
$install_command $package > /dev/null 2>&1
else
$install_command $package > /dev/null 2>&1
fi

# Configuration
#########################################
plugin="settings"
version="7.4.4"
targz_file="$plugin-$version.tar.gz"
url="https://gitlab.com/eliesat/images/-/raw/main/openatv/$targz_file"
temp_dir="/tmp"

#download & install package
#########################################
download_and_install_package() {
sleep 3
wget -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  ok=1
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

echo "> applying openatv settings ..."
sleep 3
rm -rf /tmp/file.txt
touch /tmp/file.txt

cat <<EOF >> /tmp/file.txt
config.ArabicSavior.fonts=/usr/lib/enigma2/python/Plugins/Extensions/ArabicSavior//fonts/SaberArnane-Subtitle.ttf
config.audio.volume=40
config.autolanguage.audio_autoselect1=orj dos ory org esl qaa qaf und mis mul ORY ORJ Audio_ORJ oth
config.autolanguage.audio_autoselect2=eng Englisch
config.autolanguage.audio_epglanguage_alternative=eng Englisch
config.autolanguage.subtitle_autoselect2=ara
config.av.edid_override=True
config.av.policy_169=bestfit
config.av.policy_43=bestfit
config.av.videomode.HDMI=1080p
config.av.volume_stepsize=10
config.av.volume_stepsize_fastmode=10
config.crash.lastfulljobtrashtime=1719936970
config.epg.mhw=True
config.epg.virgin=True
config.lcd.bright=8
config.lcd.dimbright=4
config.misc.ButtonSetup.back=Module/Screens.PluginBrowser/PluginBrowser
config.misc.ButtonSetup.cross_left=Infobar/volumeDown
config.misc.ButtonSetup.cross_right=Infobar/volumeUp
config.misc.ButtonSetup.epg=Infobar/toggleShow
config.misc.ButtonSetup.epg_long=Module/Screens.NetworkSetup/NetworkAdapterSelection
config.misc.ButtonSetup.info=Infobar/openBouquets
config.misc.ButtonSetup.info_long=Infobar/openSatellites
config.misc.ButtonSetup.list=Plugins/Extensions/FileCommander/1
config.misc.ButtonSetup.next=Plugins/Extensions/AJPan/9
config.misc.ButtonSetup.previous=Plugins/Extensions/HistoryZapSelector/1
config.misc.ButtonSetup.red=Plugins/Extensions/IPTVPlayer/1
config.misc.country=US
config.misc.firstrun=False
config.misc.initialchannelselection=False
config.misc.language=en
config.misc.lastrotorposition=3530
config.misc.load_unlinked_userbouquets=True
config.misc.locale=en_US
config.misc.migrationVersion=1
config.misc.nextWakeup=1719993137,-1,-1,0,0,-1,0
config.misc.SettingsVersion=1.1
config.misc.startCounter=55
config.misc.SyncTimeUsing=1
config.misc.videowizardenabled=False
config.misc.wizardLanguageEnabled=False
config.NewVirtualKeyBoard.firsttime=False
config.NewVirtualKeyBoard.lastsearchText=7.0w
config.Nims.0.advanced.lnb.1.diseqcMode=1_2
config.Nims.0.advanced.lnb.1.powerMeasurement=false
config.Nims.0.advanced.sat.100.lnb=1
config.Nims.0.advanced.sat.100.rotorposition=10
config.Nims.0.advanced.sat.100.usals=false
config.Nims.0.advanced.sat.130.lnb=1
config.Nims.0.advanced.sat.130.rotorposition=13
config.Nims.0.advanced.sat.130.usals=false
config.Nims.0.advanced.sat.160.lnb=1
config.Nims.0.advanced.sat.160.rotorposition=16
config.Nims.0.advanced.sat.160.usals=false
config.Nims.0.advanced.sat.19.lnb=1
config.Nims.0.advanced.sat.19.rotorposition=2
config.Nims.0.advanced.sat.19.usals=false
config.Nims.0.advanced.sat.192.lnb=1
config.Nims.0.advanced.sat.192.rotorposition=19
config.Nims.0.advanced.sat.192.usals=false
config.Nims.0.advanced.sat.216.lnb=1
config.Nims.0.advanced.sat.216.rotorposition=21
config.Nims.0.advanced.sat.216.usals=false
config.Nims.0.advanced.sat.235.lnb=1
config.Nims.0.advanced.sat.235.rotorposition=23
config.Nims.0.advanced.sat.235.usals=false
config.Nims.0.advanced.sat.255.lnb=1
config.Nims.0.advanced.sat.255.rotorposition=26
config.Nims.0.advanced.sat.255.usals=false
config.Nims.0.advanced.sat.260.lnb=1
config.Nims.0.advanced.sat.260.rotorposition=26
config.Nims.0.advanced.sat.260.usals=false
config.Nims.0.advanced.sat.30.lnb=1
config.Nims.0.advanced.sat.30.rotorposition=3
config.Nims.0.advanced.sat.30.usals=false
config.Nims.0.advanced.sat.305.lnb=1
config.Nims.0.advanced.sat.305.rotorposition=31
config.Nims.0.advanced.sat.305.usals=false
config.Nims.0.advanced.sat.310.lnb=1
config.Nims.0.advanced.sat.310.rotorposition=31
config.Nims.0.advanced.sat.310.usals=false
config.Nims.0.advanced.sat.315.lnb=1
config.Nims.0.advanced.sat.315.rotorposition=32
config.Nims.0.advanced.sat.315.usals=false
config.Nims.0.advanced.sat.330.lnb=1
config.Nims.0.advanced.sat.330.rotorposition=33
config.Nims.0.advanced.sat.330.usals=false
config.Nims.0.advanced.sat.3300.lnb=1
config.Nims.0.advanced.sat.3300.rotorposition=30
config.Nims.0.advanced.sat.3300.usals=false
config.Nims.0.advanced.sat.3380.lnb=1
config.Nims.0.advanced.sat.3380.rotorposition=22
config.Nims.0.advanced.sat.3380.usals=false
config.Nims.0.advanced.sat.3450.lnb=1
config.Nims.0.advanced.sat.3450.rotorposition=15
config.Nims.0.advanced.sat.3450.usals=false
config.Nims.0.advanced.sat.3460.lnb=1
config.Nims.0.advanced.sat.3460.rotorposition=14
config.Nims.0.advanced.sat.3460.usals=false
config.Nims.0.advanced.sat.3520.lnb=1
config.Nims.0.advanced.sat.3520.rotorposition=8
config.Nims.0.advanced.sat.3520.usals=false
config.Nims.0.advanced.sat.3530.lnb=1
config.Nims.0.advanced.sat.3530.rotorposition=8
config.Nims.0.advanced.sat.3530.usals=false
config.Nims.0.advanced.sat.3560.lnb=1
config.Nims.0.advanced.sat.3560.rotorposition=4
config.Nims.0.advanced.sat.3560.usals=false
config.Nims.0.advanced.sat.3592.lnb=1
config.Nims.0.advanced.sat.3592.usals=false
config.Nims.0.advanced.sat.360.lnb=1
config.Nims.0.advanced.sat.360.rotorposition=36
config.Nims.0.advanced.sat.360.usals=false
config.Nims.0.advanced.sat.390.lnb=1
config.Nims.0.advanced.sat.390.rotorposition=39
config.Nims.0.advanced.sat.390.usals=false
config.Nims.0.advanced.sat.420.lnb=1
config.Nims.0.advanced.sat.420.rotorposition=42
config.Nims.0.advanced.sat.420.usals=false
config.Nims.0.advanced.sat.450.lnb=1
config.Nims.0.advanced.sat.450.rotorposition=45
config.Nims.0.advanced.sat.450.usals=false
config.Nims.0.advanced.sat.460.lnb=1
config.Nims.0.advanced.sat.460.rotorposition=46
config.Nims.0.advanced.sat.460.usals=false
config.Nims.0.advanced.sat.48.lnb=1
config.Nims.0.advanced.sat.48.rotorposition=5
config.Nims.0.advanced.sat.48.usals=false
config.Nims.0.advanced.sat.520.lnb=1
config.Nims.0.advanced.sat.520.rotorposition=52
config.Nims.0.advanced.sat.520.usals=false
config.Nims.0.advanced.sat.525.lnb=1
config.Nims.0.advanced.sat.525.rotorposition=52
config.Nims.0.advanced.sat.525.usals=false
config.Nims.0.advanced.sat.530.lnb=1
config.Nims.0.advanced.sat.530.rotorposition=53
config.Nims.0.advanced.sat.530.usals=false
config.Nims.0.advanced.sat.549.lnb=1
config.Nims.0.advanced.sat.549.rotorposition=55
config.Nims.0.advanced.sat.549.usals=false
config.Nims.0.advanced.sat.620.lnb=1
config.Nims.0.advanced.sat.620.rotorposition=62
config.Nims.0.advanced.sat.620.usals=false
config.Nims.0.advanced.sat.70.lnb=1
config.Nims.0.advanced.sat.70.rotorposition=7
config.Nims.0.advanced.sat.70.usals=false
config.Nims.0.advanced.sat.90.lnb=1
config.Nims.0.advanced.sat.90.rotorposition=9
config.Nims.0.advanced.sat.90.usals=false
config.Nims.0.advanced.sats=216
config.Nims.0.configMode=advanced
config.Nims.0.dvbs.advanced.lnb.1.diseqcMode=1_2
config.Nims.0.dvbs.advanced.lnb.1.powerMeasurement=false
config.Nims.0.dvbs.advanced.sat.100.lnb=1
config.Nims.0.dvbs.advanced.sat.100.rotorposition=10
config.Nims.0.dvbs.advanced.sat.100.usals=False
config.Nims.0.dvbs.advanced.sat.130.lnb=1
config.Nims.0.dvbs.advanced.sat.130.rotorposition=13
config.Nims.0.dvbs.advanced.sat.130.usals=False
config.Nims.0.dvbs.advanced.sat.160.lnb=1
config.Nims.0.dvbs.advanced.sat.160.rotorposition=16
config.Nims.0.dvbs.advanced.sat.160.usals=False
config.Nims.0.dvbs.advanced.sat.19.lnb=1
config.Nims.0.dvbs.advanced.sat.19.rotorposition=2
config.Nims.0.dvbs.advanced.sat.19.usals=False
config.Nims.0.dvbs.advanced.sat.192.lnb=1
config.Nims.0.dvbs.advanced.sat.192.rotorposition=19
config.Nims.0.dvbs.advanced.sat.192.usals=False
config.Nims.0.dvbs.advanced.sat.235.lnb=1
config.Nims.0.dvbs.advanced.sat.235.rotorposition=23
config.Nims.0.dvbs.advanced.sat.235.usals=False
config.Nims.0.dvbs.advanced.sat.255.lnb=1
config.Nims.0.dvbs.advanced.sat.255.rotorposition=26
config.Nims.0.dvbs.advanced.sat.255.usals=False
config.Nims.0.dvbs.advanced.sat.260.lnb=1
config.Nims.0.dvbs.advanced.sat.260.rotorposition=26
config.Nims.0.dvbs.advanced.sat.260.usals=False
config.Nims.0.dvbs.advanced.sat.30.lnb=1
config.Nims.0.dvbs.advanced.sat.30.rotorposition=3
config.Nims.0.dvbs.advanced.sat.30.usals=False
config.Nims.0.dvbs.advanced.sat.305.lnb=1
config.Nims.0.dvbs.advanced.sat.305.rotorposition=31
config.Nims.0.dvbs.advanced.sat.305.usals=False
config.Nims.0.dvbs.advanced.sat.310.lnb=1
config.Nims.0.dvbs.advanced.sat.310.rotorposition=31
config.Nims.0.dvbs.advanced.sat.310.usals=False
config.Nims.0.dvbs.advanced.sat.330.lnb=1
config.Nims.0.dvbs.advanced.sat.330.rotorposition=33
config.Nims.0.dvbs.advanced.sat.330.usals=False
config.Nims.0.dvbs.advanced.sat.3300.lnb=1
config.Nims.0.dvbs.advanced.sat.3300.rotorposition=30
config.Nims.0.dvbs.advanced.sat.3300.usals=False
config.Nims.0.dvbs.advanced.sat.3380.lnb=1
config.Nims.0.dvbs.advanced.sat.3380.rotorposition=22
config.Nims.0.dvbs.advanced.sat.3380.usals=False
config.Nims.0.dvbs.advanced.sat.3450.lnb=1
config.Nims.0.dvbs.advanced.sat.3450.rotorposition=15
config.Nims.0.dvbs.advanced.sat.3450.usals=False
config.Nims.0.dvbs.advanced.sat.3460.lnb=1
config.Nims.0.dvbs.advanced.sat.3460.rotorposition=14
config.Nims.0.dvbs.advanced.sat.3460.usals=False
config.Nims.0.dvbs.advanced.sat.3520.lnb=1
config.Nims.0.dvbs.advanced.sat.3520.rotorposition=8
config.Nims.0.dvbs.advanced.sat.3520.usals=False
config.Nims.0.dvbs.advanced.sat.3530.lnb=1
config.Nims.0.dvbs.advanced.sat.3530.rotorposition=8
config.Nims.0.dvbs.advanced.sat.3530.usals=False
config.Nims.0.dvbs.advanced.sat.3560.lnb=1
config.Nims.0.dvbs.advanced.sat.3560.rotorposition=4
config.Nims.0.dvbs.advanced.sat.3560.usals=False
config.Nims.0.dvbs.advanced.sat.3592.lnb=1
config.Nims.0.dvbs.advanced.sat.3592.usals=False
config.Nims.0.dvbs.advanced.sat.360.lnb=1
config.Nims.0.dvbs.advanced.sat.360.rotorposition=36
config.Nims.0.dvbs.advanced.sat.360.usals=False
config.Nims.0.dvbs.advanced.sat.390.lnb=1
config.Nims.0.dvbs.advanced.sat.390.rotorposition=39
config.Nims.0.dvbs.advanced.sat.390.usals=False
config.Nims.0.dvbs.advanced.sat.420.lnb=1
config.Nims.0.dvbs.advanced.sat.420.rotorposition=42
config.Nims.0.dvbs.advanced.sat.420.usals=False
config.Nims.0.dvbs.advanced.sat.460.lnb=1
config.Nims.0.dvbs.advanced.sat.460.rotorposition=46
config.Nims.0.dvbs.advanced.sat.460.usals=False
config.Nims.0.dvbs.advanced.sat.48.lnb=1
config.Nims.0.dvbs.advanced.sat.48.rotorposition=5
config.Nims.0.dvbs.advanced.sat.48.usals=False
config.Nims.0.dvbs.advanced.sat.520.lnb=1
config.Nims.0.dvbs.advanced.sat.520.rotorposition=52
config.Nims.0.dvbs.advanced.sat.520.usals=False
config.Nims.0.dvbs.advanced.sat.525.lnb=1
config.Nims.0.dvbs.advanced.sat.525.rotorposition=52
config.Nims.0.dvbs.advanced.sat.525.usals=False
config.Nims.0.dvbs.advanced.sat.530.lnb=1
config.Nims.0.dvbs.advanced.sat.530.rotorposition=53
config.Nims.0.dvbs.advanced.sat.530.usals=False
config.Nims.0.dvbs.advanced.sat.549.lnb=1
config.Nims.0.dvbs.advanced.sat.549.rotorposition=55
config.Nims.0.dvbs.advanced.sat.549.usals=False
config.Nims.0.dvbs.advanced.sat.620.lnb=1
config.Nims.0.dvbs.advanced.sat.620.rotorposition=62
config.Nims.0.dvbs.advanced.sat.620.usals=False
config.Nims.0.dvbs.advanced.sat.70.lnb=1
config.Nims.0.dvbs.advanced.sat.70.rotorposition=7
config.Nims.0.dvbs.advanced.sat.70.usals=False
config.Nims.0.dvbs.advanced.sat.90.lnb=1
config.Nims.0.dvbs.advanced.sat.90.rotorposition=9
config.Nims.0.dvbs.advanced.sat.90.usals=False
config.Nims.0.dvbs.advanced.sats=3299
config.Nims.0.dvbs.configMode=advanced
config.Nims.0.lastsatrotorposition=3560
config.Nims.1.dvbs.configMode=nothing
config.OpenWebif.allow_upload_ipk=True
config.osd.dst_height=551
config.osd.dst_left=14
config.osd.dst_top=13
config.osd.dst_width=692
config.osd.language=en_US
config.pep.brightness=133
config.plugins.AJPanel.backupPath=/media/hdd/Ajpanel_Eliesatpanel/
config.plugins.AJPanel.browserBookmarks=/usr/lib/enigma2/python/Plugins/Extensions/,/tmp/,/
config.plugins.AJPanel.browserStartPath=/hdd/
config.plugins.AJPanel.checkForUpdateAtStartup=True
config.plugins.AJPanel.customMenuPath=$ms/Ajpanel_Eliesatpanel/
config.plugins.AJPanel.downloadedPackagesPath=/media/hdd/Ajpanel_Eliesatpanel/downloaded-packages/
config.plugins.AJPanel.exportedPIconsPath=/media/hdd/Ajpanel_Eliesatpanel/exported-picons/
config.plugins.AJPanel.exportedTablesPath=/media/hdd/Ajpanel_Eliesatpanel/exported-tables/
config.plugins.AJPanel.FileManagerExit=e
config.plugins.AJPanel.hideIptvServerChannPrefix=True
config.plugins.AJPanel.iptvAddToBouquetRefType=5002
config.plugins.AJPanel.lastCopyMoveDir=/etc/epgimport/ziko_config/
config.plugins.AJPanel.lastFeedPkgsDir=/hdd/dreamsatpanel/13-sport/
config.plugins.AJPanel.lastFileManFindSrt=/tmp
config.plugins.AJPanel.lastPkgProjDir=/etc/enigma2/
config.plugins.AJPanel.lastTerminalCustCmdLineNum=307
config.plugins.AJPanel.packageOutputPath=/media/hdd/Ajpanel_Eliesatpanel/create-package-files/
config.plugins.AJPanel.PIconsPath=/media/hdd/picon/
config.plugins.AJPanel.screenshotFType=png
config.plugins.AJPanel.subtBGTransp=60
config.plugins.AJPanel.subtDelaySec=-1
config.plugins.AJPanel.subtShadowColor=#FF0000
config.plugins.AJPanel.subtTextFg=#FFFF00
config.plugins.autoresolution.delay_switch_mode=0
config.plugins.autoresolution.enable=True
config.plugins.autoresolution.showinfo=False
config.plugins.bitrate.show_in_menu=infobar
config.plugins.bitrate.style_skin=compact
config.plugins.bitrate.x=1575
config.plugins.bitrate.y=200
config.plugins.CacheFlush.enable=True
config.plugins.CacheFlush.free_default=8192
config.plugins.CacheFlush.scrinfo=False
config.plugins.CacheFlush.sync=True
config.plugins.CacheFlush.timeout=5
config.plugins.chocholousekpicons.0.method=all_inc
config.plugins.chocholousekpicons.1.allowed=True
config.plugins.chocholousekpicons.1.background=transparent
config.plugins.chocholousekpicons.1.method=all_inc
config.plugins.chocholousekpicons.1.picon_folder_user=/media/hdd
config.plugins.chocholousekpicons.2.method=all_inc
config.plugins.chocholousekpicons.3.method=all_inc
config.plugins.epgimport.enabled=True
config.plugins.epgimport.wakeup=20:0
config.plugins.epgsearch.numorbpos=0
config.plugins.extra_epgimport.last_import=Wed Jul  3 11:19:17 2024, 18950
config.plugins.FeedsFinder.feedfreq=11500
config.plugins.FileCommander.pathLeft=media/egamiboot/
config.plugins.FileCommander.pathRight=media/egamiboot/
config.plugins.imdb.showepisodeinfo=True
config.plugins.imdb.showepisoderesults=True
config.plugins.imdb.showinplugins=True
config.plugins.imdb.showlongmenuinfo=True
config.plugins.IPAudioPro.epg=True
config.plugins.IPAudioPro.mainmenu=True
config.plugins.IPAudioPro.picons=True
config.plugins.IPToSAT.player=exteplayer3
config.plugins.iptvplayer.AktualizacjaWmenu=true
config.plugins.iptvplayer.alternativeARMV7MoviePlayer=extgstplayer
config.plugins.iptvplayer.alternativeARMV7MoviePlayer0=extgstplayer
config.plugins.iptvplayer.buforowanie_m3u8=false
config.plugins.iptvplayer.cmdwrappath=/usr/bin/cmdwrap
config.plugins.iptvplayer.debugprint=/tmp/iptv.dbg
config.plugins.iptvplayer.defaultARMV7MoviePlayer=exteplayer
config.plugins.iptvplayer.defaultARMV7MoviePlayer0=exteplayer
config.plugins.iptvplayer.dukpath=/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/bin/duk
config.plugins.iptvplayer.extplayer_infobanner_clockformat=24
config.plugins.iptvplayer.extplayer_skin=line
config.plugins.iptvplayer.f4mdumppath=/usr/bin/f4mdump
config.plugins.iptvplayer.gstplayerpath=/usr/bin/gstplayer
config.plugins.iptvplayer.opensuborg_login=MOHAMED_OS
config.plugins.iptvplayer.opensuborg_password=&ghost@mcee2017&
config.plugins.iptvplayer.osk_type=system
config.plugins.iptvplayer.plarform=armv7
config.plugins.iptvplayer.remember_last_position=true
config.plugins.iptvplayer.rtmpdumppath=/usr/bin/rtmpdump
config.plugins.iptvplayer.SciezkaCache=/etc/IPTVCache/
config.plugins.iptvplayer.skin=Adam1080p
config.plugins.iptvplayer.uchardetpath=/usr/bin/uchardet
config.plugins.iptvplayer.updateLastCheckedVersion=2024.07.02.00

config.plugins.iptvplayer.wgetpath=wget
config.plugins.JediMakerXtream.enabled=True
config.plugins.JediMakerXtream.groups=True
config.plugins.JediMakerXtream.wakeup=20:20
config.plugins.KeyAdder.Autodownload_enabled=True
config.plugins.KeyAdder.Autodownload_sitelink=MOHAMED_OS
config.plugins.KeyAdder.custom_softcampath=/var/keys/
config.plugins.KeyAdder.Show_Autoflash=True
config.plugins.KeyAdder.softcampath=True
config.plugins.KeyAdder.wakeup=20:10
config.plugins.MetrixWeather.animationspeed=80
config.plugins.MetrixWeather.currentWeatherCode=34
config.plugins.MetrixWeather.currentWeatherDataValid=3
config.plugins.MetrixWeather.currentWeatherdate=2022-07-10
config.plugins.MetrixWeather.currentWeatherday=Sunday
config.plugins.MetrixWeather.currentWeatherfeelslike=32
config.plugins.MetrixWeather.currentWeatherhumidity=66
config.plugins.MetrixWeather.currentWeatherobservationtime=16:00:00
config.plugins.MetrixWeather.currentWeathershortday=Sun
config.plugins.MetrixWeather.currentWeatherTemp=29
config.plugins.MetrixWeather.currentWeatherText=Mostly Sunny
config.plugins.MetrixWeather.currentWeatherwinddisplay=18 km/h Southwest
config.plugins.MetrixWeather.currentWeatherwindspeed=18 km/h
config.plugins.MetrixWeather.detail=True
config.plugins.MetrixWeather.forecast=5
config.plugins.MetrixWeather.forecastTodayCode=32
config.plugins.MetrixWeather.forecastTodayTempMax=35
config.plugins.MetrixWeather.forecastTodayTempMin=28
config.plugins.MetrixWeather.forecastTodayText=Sunny
config.plugins.MetrixWeather.forecastTomorrowCode=32
config.plugins.MetrixWeather.forecastTomorrowCode2=32
config.plugins.MetrixWeather.forecastTomorrowCode3=32
config.plugins.MetrixWeather.forecastTomorrowdate=2022-07-11
config.plugins.MetrixWeather.forecastTomorrowdate2=2022-07-12
config.plugins.MetrixWeather.forecastTomorrowdate3=2022-07-13
config.plugins.MetrixWeather.forecastTomorrowday=Monday
config.plugins.MetrixWeather.forecastTomorrowday2=Tuesday
config.plugins.MetrixWeather.forecastTomorrowday3=Wednesday
config.plugins.MetrixWeather.forecastTomorrowshortday=Mon
config.plugins.MetrixWeather.forecastTomorrowshortday2=Tue
config.plugins.MetrixWeather.forecastTomorrowshortday3=Wed
config.plugins.MetrixWeather.forecastTomorrowTempMax=35
config.plugins.MetrixWeather.forecastTomorrowTempMax2=35
config.plugins.MetrixWeather.forecastTomorrowTempMax3=35
config.plugins.MetrixWeather.forecastTomorrowTempMin=27
config.plugins.MetrixWeather.forecastTomorrowTempMin2=28
config.plugins.MetrixWeather.forecastTomorrowTempMin3=28
config.plugins.MetrixWeather.forecastTomorrowText=Sunny
config.plugins.MetrixWeather.forecastTomorrowText2=Sunny
config.plugins.MetrixWeather.forecastTomorrowText3=Sunny
config.plugins.MetrixWeather.icontype=1
config.plugins.MetrixWeather.owm_geocode=35.61778,33.98083
config.plugins.MetrixWeather.weathercity=Jounieh
config.plugins.MetrixWeather.weekday=true
config.plugins.MetrixWeather.woeid=2911298
config.plugins.MultiStalkerPro.adult=True
config.plugins.MultiStalkerPro.color=Moonlit Asteroid
config.plugins.MultiStalkerPro.extplayer_subtitle_font_color=#FFFF00
config.plugins.MultiStalkerPro.extplayer_subtitle_pos=70
config.plugins.MultiStalkerPro.host=http://ultraflix.uk:8080/c/
config.plugins.MultiStalkerPro.mac=00:1A:79:51:d6:1b
config.plugins.MultiStalkerPro.mainmenu=True
config.plugins.MultiStalkerPro.portalNB=4
config.plugins.MultiStalkerPro.PvrServiceType=5002
config.plugins.MultiStalkerPro.subDL=AR
config.plugins.MultiStalkerPro.tmdb_key=d48cfffeaefab8031ebc14b8dbabf146
config.plugins.MultiStalkerPro.tmdbL=ar-AR
config.plugins.MyMetrixLiteColors.channelselectionprogress=F0A30A
config.plugins.MyMetrixLiteColors.channelselectionprogressborder=FFFFFF
config.plugins.MyMetrixLiteColors.channelselectionservicedescription=BF9217
config.plugins.MyMetrixLiteColors.epgbackground=000000
config.plugins.MyMetrixLiteColors.epgbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.epgborderlines=FFFFFF
config.plugins.MyMetrixLiteColors.epgeventbackground=000000
config.plugins.MyMetrixLiteColors.epgeventbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.epgeventdescriptionbackground=000000
config.plugins.MyMetrixLiteColors.epgeventdescriptionbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.epgeventselectedbackground=000000
config.plugins.MyMetrixLiteColors.epgeventselectedbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.epgservicebackground=000000
config.plugins.MyMetrixLiteColors.epgservicebackgroundtransparency=00
config.plugins.MyMetrixLiteColors.infobaraccent1=FFFFFF
config.plugins.MyMetrixLiteColors.infobaraccent2=FFFFFF
config.plugins.MyMetrixLiteColors.infobarbackground=000000
config.plugins.MyMetrixLiteColors.infobarbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.infobarfont2=FFFFFF
config.plugins.MyMetrixLiteColors.infobarprogress=F0A30A
config.plugins.MyMetrixLiteColors.infobarprogresstransparency=00
config.plugins.MyMetrixLiteColors.layeraaccent1=FFFFFF
config.plugins.MyMetrixLiteColors.layeraaccent2=FFFFFF
config.plugins.MyMetrixLiteColors.layerabackground=000000
config.plugins.MyMetrixLiteColors.layerabackgroundtransparency=00
config.plugins.MyMetrixLiteColors.layeraextendedinfo1=FFFFFF
config.plugins.MyMetrixLiteColors.layeraextendedinfo2=FFFFFF
config.plugins.MyMetrixLiteColors.layeraprogress=F0A30A
config.plugins.MyMetrixLiteColors.layeraprogresstransparency=00
config.plugins.MyMetrixLiteColors.layeraselectionbackground=000000
config.plugins.MyMetrixLiteColors.layeraselectionbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.layeraunderline=FFFFFF
config.plugins.MyMetrixLiteColors.layerbaccent1=FFFFFF
config.plugins.MyMetrixLiteColors.layerbaccent2=FFFFFF
config.plugins.MyMetrixLiteColors.layerbbackground=000000
config.plugins.MyMetrixLiteColors.layerbbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.layerbprogress=F0A30A
config.plugins.MyMetrixLiteColors.layerbprogresstransparency=00
config.plugins.MyMetrixLiteColors.layerbselectionbackground=000000
config.plugins.MyMetrixLiteColors.layerbselectionbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.listboxborder_bottomwidth=2px
config.plugins.MyMetrixLiteColors.listboxborder_left=A4C400
config.plugins.MyMetrixLiteColors.listboxborder_right=A4C400
config.plugins.MyMetrixLiteColors.listboxborder_topwidth=2px
config.plugins.MyMetrixLiteColors.menubackground=000000
config.plugins.MyMetrixLiteColors.menubackgroundtransparency=00
config.plugins.MyMetrixLiteColors.menusymbolbackground=000000
config.plugins.MyMetrixLiteColors.menusymbolbackgroundtransparency=00
config.plugins.MyMetrixLiteColors.optionallayerhorizontalbackground=2E2E2E
config.plugins.MyMetrixLiteColors.optionallayerhorizontaltransparency=34
config.plugins.MyMetrixLiteColors.windowborder_bottom=FFFFFF
config.plugins.MyMetrixLiteColors.windowborder_left=FFFFFF
config.plugins.MyMetrixLiteColors.windowborder_right=FFFFFF
config.plugins.MyMetrixLiteColors.windowborder_top=FFFFFF
config.plugins.MyMetrixLiteColors.windowtitletextback=000000
config.plugins.MyMetrixLiteFonts.epgevent_scale=100
config.plugins.MyMetrixLiteFonts.epginfo_scale=100
config.plugins.MyMetrixLiteFonts.epgtext_scale=100
config.plugins.MyMetrixLiteFonts.globalbutton_scale=105
config.plugins.MyMetrixLiteFonts.globalclock_scale=105
config.plugins.MyMetrixLiteFonts.globallarge_scale=105
config.plugins.MyMetrixLiteFonts.globalmenu_scale=105
config.plugins.MyMetrixLiteFonts.globalsmall_scale=105
config.plugins.MyMetrixLiteFonts.globaltitle_scale=105
config.plugins.MyMetrixLiteFonts.globalweatherweek_scale=105
config.plugins.MyMetrixLiteFonts.infobarevent_scale=105
config.plugins.MyMetrixLiteFonts.infobartext_scale=100
config.plugins.MyMetrixLiteFonts.Meteo_scale=105
config.plugins.MyMetrixLiteFonts.Regular_scale=100
config.plugins.MyMetrixLiteFonts.RegularLight_scale=100
config.plugins.MyMetrixLiteFonts.screeninfo_scale=105
config.plugins.MyMetrixLiteFonts.screenlabel_scale=100
config.plugins.MyMetrixLiteFonts.screentext_scale=100
config.plugins.MyMetrixLiteFonts.SetrixHD_scale=105
config.plugins.MyMetrixLiteFonts.SkinFontExamples=preset_1
config.plugins.MyMetrixLiteOther.channelSelectionStyle=CHANNELSELECTION-4
config.plugins.MyMetrixLiteOther.EHDenabled=1
config.plugins.MyMetrixLiteOther.EHDoldlinechanger=true
config.plugins.MyMetrixLiteOther.EHDrounddown=True
config.plugins.MyMetrixLiteOther.movielistStyle=right
config.plugins.MyMetrixLiteOther.piconresize_experimental=True
config.plugins.MyMetrixLiteOther.piconsharpness_experimental=5.00
config.plugins.MyMetrixLiteOther.runningTextSpeed=60
config.plugins.MyMetrixLiteOther.runningTextStartdelay=1200
config.plugins.MyMetrixLiteOther.setTunerManual=1
config.plugins.MyMetrixLiteOther.showChannelName=False
config.plugins.MyMetrixLiteOther.showChannelNumber=False
config.plugins.MyMetrixLiteOther.showExtended_caid=False
config.plugins.MyMetrixLiteOther.showExtendedinfo=True
config.plugins.MyMetrixLiteOther.showInfoBarResolutionExtended=True
config.plugins.MyMetrixLiteOther.showMovieListScrollbar=True
config.plugins.MyMetrixLiteOther.showRAMfree=True
config.plugins.MyMetrixLiteOther.showSTBinfo=True
config.plugins.MyMetrixLiteOther.SkinDesignButtons=True
config.plugins.MyMetrixLiteOther.SkinDesignButtonsBackColor=000000
config.plugins.MyMetrixLiteOther.SkinDesignButtonsTextColor=FFFFFF
config.plugins.MyMetrixLiteOther.SkinDesignButtonsTextFont=/usr/share/fonts/ae_AlMateen.ttf
config.plugins.MyMetrixLiteOther.SkinDesignInfobarZZZPiconPosX=0
config.plugins.MyMetrixLiteOther.SkinDesignInfobarZZZPiconPosY=0
config.plugins.MyMetrixLiteOther.SkinDesignInfobarZZZPiconSize=0
config.plugins.MyMetrixLiteOther.SkinDesignLLCheight=720
config.plugins.MyMetrixLiteOther.SkinDesignLLCposz=1
config.plugins.MyMetrixLiteOther.SkinDesignLLCwidth=795
config.plugins.MyMetrixLiteOther.SkinDesignLUCheight=41
config.plugins.MyMetrixLiteOther.SkinDesignLUCposz=1
config.plugins.MyMetrixLiteOther.SkinDesignLUCwidth=200
config.plugins.MyMetrixLiteOther.SkinDesignOLH=screens
config.plugins.MyMetrixLiteOther.SkinDesignOLHheight=670
config.plugins.MyMetrixLiteOther.SkinDesignOLHposx=30
config.plugins.MyMetrixLiteOther.SkinDesignOLHposy=15
config.plugins.MyMetrixLiteOther.SkinDesignOLHposz=1
config.plugins.MyMetrixLiteOther.SkinDesignOLHwidth=1220
config.plugins.MyMetrixLiteOther.SkinDesignOLVheight=3
config.plugins.MyMetrixLiteOther.SkinDesignOLVposx=0
config.plugins.MyMetrixLiteOther.SkinDesignOLVposy=696
config.plugins.MyMetrixLiteOther.SkinDesignOLVposz=1
config.plugins.MyMetrixLiteOther.SkinDesignOLVwidth=1280
config.plugins.MyMetrixLiteOther.SkinDesignRLCheight=101
config.plugins.MyMetrixLiteOther.SkinDesignRLCwidth=1280
config.plugins.MyMetrixLiteOther.SkinDesignRUCheight=720
config.plugins.MyMetrixLiteOther.SkinDesignRUCposz=1
config.plugins.MyMetrixLiteOther.SkinDesignRUCwidth=1280
config.plugins.OAWeather.owm_geocode=35.61778,33.98083
config.plugins.OAWeather.weathercity=Jounieh
config.plugins.PermanentClock.color_digital=2
config.plugins.PermanentClock.enabled=True
config.plugins.PermanentClock.position_x=1551
config.plugins.PermanentClock.position_y=0
config.plugins.serviceapp.servicemp3.player=exteplayer3
config.plugins.serviceapp.servicemp3.replace=True
config.plugins.servicescanupdates.add_new_radio_services=False
config.plugins.servicescanupdates.clear_bouquet=True
config.plugins.subtitlesSupport.encodingsGroup=Arabic
config.plugins.subtitlesSupport.external.font.size=52
config.plugins.subtitlesSupport.search.edna_cz.enabled=False
config.plugins.subtitlesSupport.search.itasa.enabled=False
config.plugins.subtitlesSupport.search.lang1=ar
config.plugins.subtitlesSupport.search.lang2=ar
config.plugins.subtitlesSupport.search.lang3=ar
config.plugins.subtitlesSupport.search.mysubs.enabled=False
config.plugins.subtitlesSupport.search.opensubtitles.enabled=False
config.plugins.subtitlesSupport.search.podnapisi.enabled=False
config.plugins.subtitlesSupport.search.prijevodionline.enabled=False
config.plugins.subtitlesSupport.search.serialzone_cz.enabled=False
config.plugins.subtitlesSupport.search.subscene.enabled=False
config.plugins.subtitlesSupport.search.subtitles_gr.enabled=False
config.plugins.subtitlesSupport.search.subtitlist.enabled=False
config.plugins.subtitlesSupport.search.titlovi.enabled=False
config.plugins.subtitlesSupport.search.titulky_com.enabled=False
config.plugins.torreplayer.rememberlastsearch=True
config.plugins.torreplayer.rememberlastsearchyts=True
config.plugins.torreplayer.rememberlastsearchyts_tv=True
config.plugins.torreplayer.tmdblng=en
config.plugins.translator.destination=ar
config.plugins.weathermsn.city=Jounieh,Lebanon,Keserwan-Jbeil Governorate
config.plugins.WeatherPlugin.Entry.0.city=Jounieh, Lebanon
config.plugins.WeatherPlugin.Entry.0.weatherlocationcode=wc:LEXX0005
config.plugins.WeatherPlugin.entrycount=1
config.plugins.XKlass.defaultplaylist=sf.bleutv.net
config.plugins.XKlass.livetype=5002
config.plugins.XKlass.subs=True
config.plugins.XKlass.vodtype=5002
config.plugins.xtraEvent.apis=True
config.plugins.xtraEvent.deletFiles=False
config.plugins.xtraEvent.extra3=True
config.plugins.xtraEvent.FanartSearchType=movies
config.plugins.xtraEvent.loc=/media/hdd/
config.plugins.xtraEvent.searchMANUELnmbr=0
config.plugins.xtraEvent.searchMOD=Current Channel
config.plugins.xtraEvent.searchNUMBER=8
config.plugins.xtraEvent.searchType=movie
config.plugins.xtraEvent.timerHour=2
config.plugins.xtraEvent.timerMod=Period
config.plugins.xtraEvent.tmdbAPI=c7ca0c239088f1ae72a197d1b4be51b8
config.plugins.xtraEvent.tvdb=True
config.plugins.xtraEvent.tvdbAPI=a99d487bb3426e5f3a60dea6d3d3c7ef
config.skin.primary_skin=MetrixHD/skin.MySkin.xml
config.softcam.showInExtensions=True
config.subtitles.ai_enabled=True
config.subtitles.ai_subtitle_colors=2
config.subtitles.ai_translate_to=ar
config.subtitles.dvb_subtitles_yellow=True
config.subtitles.pango_subtitle_colors=2
config.subtitles.ttx_subtitle_colors=2
config.timeshift.skipreturntolive=True
config.timezone.area=Asia
config.timezone.val=Beirut
config.tv.lastroot=1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "bouquets.tv" ORDER BY bouquet;1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.7_0w_ku_band_nilesat_201_301___eutelsat_7_west_a___services__tv_.tv" ORDER BY bouquet:7.0W Nilesat 201/301 & Eutelsat 8 West B;
config.tv.lastservice=1:0:1:8:9C68:77E:DCA0000:0:0:0:
config.usage.alternateGitHubDNS=True
config.usage.alternative_number_mode=True
config.usage.boolean_graphic=True
config.usage.crypto_icon_mode=1
config.usage.date.compact=%-d %b 
config.usage.date.compressed=%-d%b 
config.usage.date.displayday=%a %-d %b
config.usage.dns=google
config.usage.frontend_priority=expert_mode
config.usage.informationExtraSpacing=True
config.usage.last_movie_played=1:0:0:0:0:0:0:0:0:0:http%3a//esuntv.com%3a88/02025843197390/Miozi9bgUo/375934:BEIN MOVIES HD1 PREMIERE
config.usage.menu_sort_weight={'mainmenu': {'submenu': {'information': {'sort': 60}, 'timermenu': {'sort': 70}, 'plugin_selection': {'sort': 80}, 'setup': {'sort': 100}, 'shutdown': {'sort': 120}, 'egami_boot': {'sort': 20}, 'JediMakerXtream': {'sort': 30, 'hidden': True}, 'Listen to your favorite commentators': {'sort': 40, 'hidden': True}, 'XStreamity': {'sort': 110, 'hidden': True}, 'beengo': {'sort': 50, 'hidden': True}, 'BouquetMakerXtream': {'sort': 150, 'hidden': True}, 'hdf_radio': {'sort': 110, 'hidden': True}, 'ipsat': {'sort': 10, 'hidden': True}, 'ipaudioplus': {'sort': 20, 'hidden': True}, 'neo_boot': {'sort': 30, 'hidden': True}, 'NeoReboot': {'sort': 170}, 'novatv': {'sort': 60, 'hidden': True}, 'novacampro': {'sort': 70, 'hidden': True}, 'novalerstore': {'sort': 80, 'hidden': True}, 'novalertv': {'sort': 90, 'hidden': True}, 'ultracam': {'sort': 40, 'hidden': True}, 'XCplugin': {'sort': 100, 'hidden': True}, 'XKlass': {'sort': 90, 'hidden': True}, 'Multi-StalkerPro': {'sort': 10, 'hidden': True}, 'Levi45 Addon Manager': {'sort': 50, 'hidden': True}}}}
config.usage.menuEntryStyle=both
config.usage.numzappicon=True
config.usage.okbutton_mode=1
config.usage.panicbutton=True
config.usage.progressinfo_fontsize=-2
config.usage.quickzap_bouquet_change=True
config.usage.recording_frontend_priority=-1
config.usage.screen_saver=300
config.usage.service_icon_enable=True
config.usage.serviceinfo_fontsize=-2
config.usage.servicelist_picon_downsize=0
config.usage.servicelistpreview_mode=True
config.usage.servicename_fontsize=2
config.usage.servicenum_fontsize=2
config.usage.servicetype_icon_mode=1
config.usage.show_event_progress_in_servicelist=barleft
config.usage.show_infobar_channel_number=True
config.usage.show_second_infobar=2
config.usage.showInfoBarSubservices=2
config.usage.showScreenPath=small
config.usage.shutdownOK=False
config.usage.swap_snr_on_osd=True
config.usage.updownbutton_mode=0
EOF
#device
device=$(head -n 1 /etc/hostname)
echo "config.plugins.MyMetrixLiteOther.EHDtested="$device"_|_01" >> /tmp/file.txt

sleep 3
echo "> your device will reboot now please wait..."
init 4
sleep 2
mv /tmp/file.txt /etc/enigma2/settings
if [ "$it" = "DreamOS" ]; then
sleep 2
systemctl restart enigma2
else
sleep 2
reboot
fi
